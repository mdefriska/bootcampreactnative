console.log("Jawaban soal no.1:");
function teriak() {
  return "Halo Sanbers!\n";
}
console.log(teriak());

console.log("Jawaban soal no.2:");
function kalikan(x,y) {
  return x*y;
}
 
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("Jawaban soal no.3:");
function introduce(name,age,address,hobby) {
  return "\nNama saya "+ name + ", umur saya " + age + " tahun, alamat saya di " + address + " ,dan saya punya hobby yaitu " + hobby + "!";
}
 
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

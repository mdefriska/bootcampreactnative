let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function baca(time, books, i){
	if(i<books.length){
		readBooksPromise(time, books[i])
			.then(function(sisa){
				i++;
				baca(sisa, books, i);
			})
			.catch(function(error){
				
			});
	}
}

baca(10000, books,0)
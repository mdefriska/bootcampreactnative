import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

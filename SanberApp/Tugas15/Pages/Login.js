import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text>Login</Text>
            <Button 
                onPress={()=>navigation.navigate('MyDrawer',{
                    Screen : 'App', param:{
                        Screen:'AboutScreen'
                    }
                })}
                title='HomeScreen' />

        </View>
    )
}

const styles = StyleSheet.create({})

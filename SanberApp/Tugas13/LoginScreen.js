import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import { FontAwesome} from '@expo/vector-icons';

export default class LoginScreen extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.logo}>
                        <Image
                            source={require('./images/logo.png')}
                        />
                    </View>
                    <View style={styles.title}>
                            <Text style={styles.titleText}>Sign In</Text>
                    </View>
                </View>
                <View style={styles.form}>
                    <View style={styles.itemForm}>
                        <FontAwesome name="user-circle" size={20} color='white' />
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Username'
                            placeholderTextColor='white'
                        />
                    </View>
                    <View style={styles.itemForm}>
                        <FontAwesome name="lock" size={28} color='white' />
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Password'
                            placeholderTextColor='white'
                        />
                        <TouchableOpacity>
                            <Text style={{color:'white',marginTop:6}}>forgot!</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                            style={styles.button}
                        >
                            <Text style={styles.buttonText}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.signup}>
                        <Text style={{color:'white'}}>Don't have an account? </Text>
                        <TouchableOpacity>
                            <Text style={{color:'white', fontWeight:'bold'}}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
		    </View>
    )
  }
}

const styles = StyleSheet.create({
	container:{
		flex:1,
        backgroundColor: '#2991FF',
        flexDirection:'column'
	},
	header:{
		backgroundColor:'#2991FF',
		marginTop:100,
        flexDirection: 'column',
	},
	logo:{
        alignItems: 'center',
        justifyContent:'center',
	},
    title:{
        justifyContent:'center',
        marginLeft:33,
        marginTop:50
    },
    titleText:{
        color:'white', 
        fontSize:25, 
        fontWeight: 'bold',
    },
    form:{
        marginTop:20
    },
    itemForm:{
        flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'white',
        marginTop: 20,
        marginLeft : 30,
        marginRight : 30,
        marginBottom : 15
    },
    buttonView:{
        marginLeft:30,
        marginRight:30,
        marginTop: 55
    },
    button:{
        backgroundColor:'white',
        height:35,
        width: 300,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText:{
        color: '#2991FF',
        fontWeight:'bold',
        fontSize:17,   
    },
    signup:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        marginTop:60
    },
})
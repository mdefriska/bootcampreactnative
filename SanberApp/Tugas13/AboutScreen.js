import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import { FontAwesome5} from '@expo/vector-icons';

export default class AboutScreen extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.subHeader}>
                        <Image
                            style={{height:130, width:130}}
                            source={require('./images/user.png')}
                        />
                    </View>
                    <View style={styles.nama}>
                            <Text style={styles.namaText}>Mohamad Firman Maulana</Text>
                            <Text style={styles.asText}>React Native Developer</Text>
                    </View>
                </View>
                <View style={styles.form}>
                    <View style={styles.itemForm}>
                        <TouchableOpacity style={styles.touchItemForm}>
                        <FontAwesome5 name="facebook" size={30} color='#2991FF' />
                        <Text style={styles.itemText}>
                            Mohamad Firman Maulana
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.itemForm}>
                        <TouchableOpacity style={styles.touchItemForm}>
                        <FontAwesome5 name="instagram" size={30} color='#2991FF' />
                        <Text style={styles.itemText}>
                            @firman.mohamad
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.itemForm}>
                        <TouchableOpacity style={styles.touchItemForm}>
                        <FontAwesome5 name="twitter" size={24} color='#2991FF' />
                        <Text style={styles.itemText}>
                            @firman.mohamad
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.itemForm}>
                        <TouchableOpacity style={styles.touchItemForm}>
                        <FontAwesome5 name="whatsapp" size={28} color='#2991FF' />
                        <Text style={styles.itemText}>
                            0856 2222 3333
                        </Text>
                        </TouchableOpacity>
                    </View>
                   <View style={styles.portofolio}>
                       <View style={styles.judul}>
                           <Text style={styles.portoText}>Portofolio</Text>
                       </View>
                       <View style={styles.listPorto}>
                           <View style={styles.itemListPorto}> 
                                <FontAwesome5 name="github" size={60} color='white' />
                            </View>
                            <View style={styles.itemListPorto}>
                                <FontAwesome5 name="gitlab" size={60} color='white' />
                            </View>
                       </View>
                   </View>
                    
                </View>
		    </View>
    )
  }
}

const styles = StyleSheet.create({
	container:{
		flex:1,
        backgroundColor: 'white',
        flexDirection:'column'
	},
	header:{
		backgroundColor:'white',
		marginTop:100,
        flexDirection: 'column',
	},
	subHeader:{
        alignItems: 'center',
        justifyContent:'center',
	},
    nama:{
        justifyContent:'center',
        alignItems: 'center',
        marginTop:10,
        marginBottom :15
    },
    namaText:{
        color:'#2991FF', 
        fontSize:25, 
        fontWeight: 'bold',
        borderBottomWidth:1,
        borderBottomColor: '#2991FF',
        marginLeft:30,
        marginRight:30
    },
    asText:{
        color:'#2991FF', 
        fontSize:25, 
    },
    form:{
        marginTop:15,
    },
    itemForm:{
        borderBottomWidth:1,
        borderBottomColor:'white',
        marginTop:5,
        marginLeft : 50,
        marginRight : 30,
        marginBottom : 15
    },
    itemText:{
        paddingLeft:15,
        fontSize: 18,
        color: '#2991FF',
        marginTop:5
    },
    touchItemForm:{
        flexDirection: 'row',
    },
    buttonView:{
        marginLeft:30,
        marginRight:30,
        marginTop: 55
    },
    button:{
        backgroundColor:'white',
        height:35,
        width: 300,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText:{
        color: '#2991FF',
        fontWeight:'bold',
        fontSize:17,   
    },
    signup:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        marginTop:60
    },
    portofolio:{
        marginTop:25,
        marginLeft:30,
        marginRight:30,
        width:300,
        height:150,
        backgroundColor:'#A9D3FF',
        borderBottomWidth:1,
        borderColor:'#A9D3FF',
        borderRadius:10,

    },
    judul:{
        borderColor:'#2991FF',
        backgroundColor:'#2991FF',
        width:100,
        height:30,
        alignItems:'center',
        justifyContent:'center',
        marginLeft:10,
        marginTop: -10
    },
    portoText:{
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
    },
    listPorto:{
        flexDirection:'row',
        paddingLeft:10
    },
    imagePorto:{
        paddingRight:10,
        height: 25,
        width: 25
    },
    itemListPorto:{
        padding: 10
    }
})
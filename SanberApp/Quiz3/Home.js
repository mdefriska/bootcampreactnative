import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, ScrollView } from 'react-native'
import { FlatList } from 'react-native-gesture-handler';
import { Data }from './data'
export default function Home({navigation, route}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

     const currencyFormat=(num)=> {
         return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
       };

    const updateHarga =(price)=>{
         console.log("UpdatPrice : " + price);
         const temp = Number(price) + totalPrice;
         console.log(temp)
         setTotalPrice(temp) 
    }
  
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang, </Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <ScrollView>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
                <FlatList
                    data={Data}
                    keyExtractor={(item)=>item.id}
                    renderItem={({item})=>{
                        return( 
                            <View style={{flexDirection:'column'}}>     
                            <View style={styles.content}>
                                <Text>{item.title}</Text>
                                <Image 
                                    source={item.image}
                                    style={{height:50, width:50}} />
                                <Text>{item.harga}</Text>
                                <Text>{item.type}</Text>
                                <Button title='BELI' onPress={()=>{updateHarga(item.harga)}}/>
                            </View>    
                            </View>                    
                        )
                    }
                }
                />             
            </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 140,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})

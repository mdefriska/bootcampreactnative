import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";

export default function Login({ navigation }) {
   let [username, setUsername] = useState("");
   let [password, setPassword] = useState("");
   let [isError, setIsError] = useState(false);

  const submit = (username, password) => {
      if(password=='12345678'){
          navigation.navigate("Home", {username :username })
      }
      else{
        isError=true
      }
    
  };
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: "bold" }}>== Quiz 3 ==</Text>
      <Image
        style={{ height: 150, width: 150 }}
        source={require("./assets/logo.jpg")}
      />
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
           value={username}
           onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Button onPress={submit(username, password)} title="Login" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
});

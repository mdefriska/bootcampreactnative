console.log("Jawaban soal no.1:");
function arrayToObject(arr) {
  if(arr==null){
	  return "";
  }
  else{
  var now = new Date();
  var thisYear = now.getFullYear();
  var personObj = {
	  firstName: "",
      lastName: "",
      gender: "",
      age: 0,
  }
	  
  for (var i=0 ; i<arr.length ; i++){
	  personObj.firstName = arr[i][0];
	  personObj.lastName = arr[i][1];
	  personObj.gender = arr[i][2];
	  var usia = thisYear - arr[i][3];
	  if (usia < 0 || arr[i][3]==null){
		  personObj.age = "Invalid birth year";
	  }
	  else{
		  personObj.age = usia;
	  }
	  var nama = personObj.firstName +" "+ personObj.lastName;
	  console.log(i+1+". "+nama+" :", personObj);
	  }
  }
}
var people 
= [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) ;
arrayToObject([]);

console.log("\nJawaban soal no.2:");
function shoppingTime(memberId, totalMoney) {
	var belanja=[];
	var total =0;
	var money = totalMoney;
	if(memberId=="" || memberId==null){
		return "Mohon maaf, toko X hanya berlaku untuk member saja";
	}
	else if(totalMoney<50000){
		return "Mohon maaf, uang tidak cukup";
	}
	else{
		if (money>=1500000){
			belanja.push("Sepatu Stacattu");
			money -= 1500000;
		}
		if (money>=500000){
			belanja.push("Baju Zoro");
			money -= 500000;
		}
		if (money>=250000){
			belanja.push("Baju H&N");
			money -= 250000;
		}			
		if (money>=175000){
			belanja.push("Sweater Uniklooh");
			money -= 175000;
		}
		if (money>=50000){
			belanja.push("Casing Handphone");
			money -= 50000;
		}
		var barang={
			memberId : memberId,
			money : totalMoney,
			listPurchased : belanja,
			changeMoney: money
		}
		return barang;
	}
}
 
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

console.log("\nJawaban soal no.3:");
function naikAngkot(arr) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var semua = [];
  if(arr==null || arr==""){
	  return semua;
  }
  else{
  for (var i=0 ; i<arr.length ; i++){
	var ong = (rute.indexOf(arr[i][2])-rute.indexOf(arr[i][1])) * 2000;
	semua[i] = {"penumpang" : arr[i][0], "naikDari" : arr[i][1], "tujuan" :			   arr[i][2], "bayar" : ong};
  }
  return semua;
  }
}
 
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]
  